import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email: string;
  name: string;
  password: string;
  code: string;
  message: string;

  constructor(private authService:AuthService, private router:Router){}



  signUp() {
    this.authService.signUp(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name);
      this.authService.addUser(value.user, this.name, this.email);
    }).then(value => {
      this.router.navigate(['/welcome']);
    })    .catch(err => {
      this.code = err.code;
      this.message = err.message;
      console.log(err);
    })
  }
 
  
  ngOnInit() {
  }


 
 
}