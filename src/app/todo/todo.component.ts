import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {


  @Input() data:any;
  @Output() myButtonClicked = new EventEmitter<any>();

  text;
  tempText;
  key;
  checkboxFlag: boolean;

  showButton = false;
  showEditField = false;

  showEdit()
  {
    this.tempText = this.text;
    this.showButton = false;
    this.showEditField = true;
  }

  save()
  {
    this.todosService.updateTodo(this.key,this.text, this.checkboxFlag);
    this.showEditField = false;
  }

  cancel()
  {
    this.text = this.tempText;
    this.showEditField = false;
  }

  checkChange()
  {
    this.todosService.updateDone(this.key,this.text,this.checkboxFlag);
  }

  buttonOn() {
    this.showButton = true;
  }
 
  buttonOff() {
    this.showButton = false;
  }

  deleteTodo()
  {
    this.todosService.deleteTodo(this.key);
  }


  constructor(private todosService: TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.key = this.data.$key;
    this.checkboxFlag = this.data.done;
  }


}