import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  addTodo(text:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/todos').push({'text':text, 'done':false});
    })
  }

  deleteTodo(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/todos').remove(key);
    })
  }

  updateTodo(key:string, text:string, done:boolean)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
    })
  }

  updateDone(key:string, text:string, done:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/uid/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
    })
    
  }

  constructor(private authService: AuthService,
              private db: AngularFireDatabase)  { }
}