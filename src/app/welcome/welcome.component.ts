import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ActivatedRoute } from "@angular/router";
import { AngularFireAuth } from '@angular/fire/auth';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { TodosService } from '../todos.service';



@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  name = '';
  text: string;
  user;
  todos: any[];
  ifStatus:boolean;
  todoStatus:boolean;
  show = false;
  done: string;

  /// Active filter rules
  filters = {}
  

  constructor(private db: AngularFireDatabase, 
              public authService: AuthService, 
              private route: ActivatedRoute,
              private router:Router, 
              // public afAuth: AngularFireAuth,
              private todosService: TodosService ) { this.showTodo(); }



  showTodo()
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }  
      )
    })
  }

   addTodo(){
    this.todosService.addTodo(this.text);
    this.text = '';
    this.showTodo();
  }


  onLogout()
  {
    this.authService.logOut().
    then(value =>{
    this.router.navigate(['/signup'])
    }).catch(err=> {
      console.log(err)
    })
  }
  toFilter()
  {
    this.authService.user.subscribe(user => {
    this.db.list('/users/uid/'+user.uid+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;          
              if (this.ifStatus == y['status']) {
                this.todos.push(y);
              }
              else if (this.ifStatus != true && this.ifStatus != false)
              {
                this.todos.push(y);
              }
              
          }
        )
      }
    )
    })
  }
  ngOnInit() {
    // this.getCurrentUser();
    this.authService.user.subscribe(user => {
      this.db.list('/users/uid/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }
      )
    })
  }

}
