import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {AuthService} from '../auth.service';
@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router,public authService:AuthService) { }

  ngOnInit() {
  }

  toLogin() {
    this.router.navigate(['/login'])
  }

  toSignUp() {
    this.router.navigate(['/signup'])
  }

  toWelcome() {
    this.router.navigate(['/welcome'])
  }
  toLogout(){
    this.authService.logOut().
    then(value =>{
      this.router.navigate(['/login'])
    }).catch(err=>{console.log(err)})
  }
}
